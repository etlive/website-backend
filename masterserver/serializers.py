from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework import viewsets
from models import Gameserver

class ServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gameserver
        fields = ('id', 'host_address', 'host_name', 'max_clients',
                  'num_players', 'map_name', 'gametype')

class ServersViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    serializer_class = ServerSerializer
    queryset = Gameserver.objects.all()

    def get_random_set(self):
        from itertools import chain
        from random import randint
        qset = Gameserver.objects.all()
        for model in qset:
            model.id = (randint(20, 200) * randint(3, 300))
            model.num_players = model.id
            model.host_name = "RandHostname %s"%model.id
        return qset

    def get_queryset(self):
        queryset = Gameserver.objects.all()

        # uncomment to return a larger, 'randomized' set
        #queryset = list(chain(self.get_random_set(), self.get_random_set(), self.get_random_set(), self.get_random_set(), self.get_random_set(), self.get_random_set(), self.get_random_set(), self.get_random_set(), self.get_random_set(), self.get_random_set()))
        return queryset


class SimpleUserSerializer( serializers.ModelSerializer ):
    class Meta:
        model = User
        fields = ('id', 'username')
