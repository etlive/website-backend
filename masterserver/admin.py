from django.contrib import admin

# Register your models here.
from models import Serverkey, Clientkey, Gameserver


class CreatedModifiedAdmin(admin.ModelAdmin):
    readonly_fields = ("created", "updated",)

class GameserverAdmin(admin.ModelAdmin):
    readonly_fields = ("created", "updated",)
    list_display = ('id', 'cleaned_hostname', 'updated', 'map_name', 'num_players', 'max_clients', 'gametype', 'needspass', 'host_address', 'status')

class ClientkeyAdmin(admin.ModelAdmin):
    readonly_fields = ("created", "updated",)
    list_display = ('id', 'user', 'status', 'server')


admin.site.register(Serverkey, CreatedModifiedAdmin)
admin.site.register(Clientkey, ClientkeyAdmin)
admin.site.register(Gameserver, GameserverAdmin)

