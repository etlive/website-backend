import uuid
import base64

from datetime import datetime, timedelta

from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny

from models import Clientkey, Serverkey, Gameserver
from serializers import SimpleUserSerializer

def address_from_request(request):
    try:
        # case proxy forward
        client_address = request.META['HTTP_X_FORWARDED_FOR']
    except:
        # case others
        client_address = request.META['REMOTE_ADDR']
    return client_address

class GetClientkeyAPI(APIView):
    """ gets a client key for a specific session/connection """
    model = Clientkey
    def deleteKey(self, user):
        try:
            clientkeyObject = Clientkey.objects.get(user=user)
            clientkeyObject.delete()
        except ObjectDoesNotExist:
            pass
    
    def get(self, request, *args, **kwargs):
        server_address = kwargs['server_address']
        try:
            gameserver = Gameserver.objects.get(host_address=server_address)
        except ObjectDoesNotExist:
            return Response("Server key could not be found", status=status.HTTP_404_NOT_FOUND)
        
        # delete any existing key
        self.deleteKey(request.user)

        # first delete all existing keys from this user
        Clientkey.objects.filter(user=request.user).delete()
        
        # create a new key and return it
        key = base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')
        expires = datetime.now() + timedelta(minutes=10)
        clientkey = Clientkey(user=request.user, server=gameserver, key=key, expires=expires, status='pending')
        clientkey.save()

        retObj = { "etl_session": key }
        return Response(retObj, status=status.HTTP_200_OK)

class ConnectClientAPI(APIView):
    """ validates a client key and returns the username """
    model = Clientkey
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        server_key = kwargs['server_key']
        try:
            serverkeyObject = Serverkey.objects.get(key=server_key)
            if(serverkeyObject.valid == False):
                raise ObjectDoesNotExist 
        except ObjectDoesNotExist:
            return Response({"detail":"Server key could not be found"}, status=status.HTTP_404_NOT_FOUND)
    
        port = kwargs['port']
        client_key = kwargs['client_key']
        try:
            clientkeyObject = Clientkey.objects.get(key=client_key)
        except ObjectDoesNotExist:
            return Response({"detail":"Client key could not be found"}, status=status.HTTP_404_NOT_FOUND)
    
        server_address = address_from_request(request)
        gameserver_addr = '%s:%s' % (server_address, port) 

        if(clientkeyObject.server.host_address != gameserver_addr):
            return Response({"detail":"Client key is invalid"}, status=status.HTTP_401_UNAUTHORIZED)

        # update the client key
        clientkeyObject.status = 'connected'
        clientkeyObject.save()

        userserializer = SimpleUserSerializer(clientkeyObject.user)
        return Response(userserializer.data)

class DisconnectClientAPI(APIView):
    """ used to invalidate a client session and optionally update statistics """
    model = Clientkey
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        client_key = kwargs['client_key']
        try:
            clientkeyObject = Clientkey.objects.get(key=client_key)
        except ObjectDoesNotExist:
            return Response({"detail":"Client key could not be found"}, status=status.HTTP_404_NOT_FOUND)
    
        # remove the client key
        clientkeyObject.delete()

        return Response("", status=status.HTTP_200_OK)


class HeartbeatAPI(APIView):
    """ used for sending a heartbeat from the gameserver, requires a valid serverkey """
    model = Clientkey
    permission_classes = (AllowAny,)

    def getServerKey(self, key):
        try:            
            serverkey = Serverkey.objects.get(key=key)
            return serverkey
        except ObjectDoesNotExist:
            return None
    
    def get(self, request, *args, **kwargs):
        serverkey = kwargs['server_key']
        port = kwargs['port']
        serverStatus = kwargs['status'] # up/down
        hostAddress = '%s:%s' % (address_from_request(request), port)
                    
        key = self.getServerKey(serverkey)
        if key is None:
            return Response({"detail":"Could not find a serverkey that matches the provided key"}, status=status.HTTP_401_UNAUTHORIZED)
        elif key.valid is False:
            return Response({"detail":"Invalid serverkey"}, status=status.HTTP_401_UNAUTHORIZED)
            
        try:
            # also check the hostaddress
            gameserver = Gameserver.objects.get(serverkey__key=serverkey, host_address=hostAddress)
        except ObjectDoesNotExist:
            gameserver = Gameserver(host_address=hostAddress)
            gameserver.serverkey = key

        try:    
            gameserver.status = serverStatus
            gameserver.save()
        except Exception as e:
            return Response({"detail":"Error saving the new status: %s"%e}, status=status.HTTP_401_UNAUTHORIZED)

        return Response("", status=status.HTTP_200_OK)

