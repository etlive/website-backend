from django.conf.urls import url, patterns, include
from django.views.generic import TemplateView
from django.conf.urls.static import static

from rest_framework import viewsets, routers

from serializers import ServersViewSet
from api import GetClientkeyAPI, ConnectClientAPI, DisconnectClientAPI, HeartbeatAPI

router = routers.DefaultRouter(trailing_slash=True)
router.register(r'servers', ServersViewSet)

urlpatterns = patterns('masterserver.views',
    url(r'^getkey/(?P<server_address>.+)', GetClientkeyAPI.as_view()),
    url(r'^connect/(?P<server_key>[-\w]+)/(?P<port>[0-9]+)/(?P<client_key>.+)', ConnectClientAPI.as_view()),
    url(r'^disconnect/(?P<server_key>[-\w]+)/(?P<port>[0-9]+)/(?P<client_key>.+)', DisconnectClientAPI.as_view()),
    
    url(r'^heartbeat/(?P<server_key>[-\w]+)/(?P<port>[0-9]+)/(?P<status>[-\w]+)', HeartbeatAPI.as_view()),
)