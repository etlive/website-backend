import datetime

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.contrib import admin

from q3util import filter_name

# in case we're going to use a custom model, use get_user_model:
#from django.contrib.auth import get_user_model
#User = get_user_model()

class Serverkey(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    key = models.CharField(max_length=255)
    valid = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, default=datetime.datetime.now)
    updated = models.DateTimeField(auto_now=True, default=datetime.datetime.now)
    def __str__(self):
        return "[%s] KEY=%s - %s" % (self.user, self.key, "VALID" if self.valid else "INVALID")

class Gameserver(models.Model):
    # gameserver fields(populated by getinfo)
    host_address = models.CharField(max_length=255)
    host_name = models.CharField(max_length=50)
    max_clients = models.IntegerField(default=0)
    num_players = models.IntegerField(default=0)
    map_name = models.CharField(max_length=50,default="")
    gametype = models.IntegerField(default=5)    
    game = models.CharField(max_length=50, default="")
    version = models.CharField(max_length=50, default="")
    needspass = models.BooleanField(default=False)

    # other fields
    serverkey = models.ForeignKey(Serverkey, default=-1)
    status = models.CharField(max_length=255, default='')        
        
    created = models.DateTimeField(auto_now_add=True, default=datetime.datetime.now)
    updated = models.DateTimeField(auto_now=True, default=datetime.datetime.now)

    def players(self):
        return Clientkey.objects.all().filter(server=self.pk, status='connected')
    
    def cleaned_hostname(self):
        return filter_name(self.host_name)

    def __str__(self):
        return "%s - %s" % (self.cleaned_hostname(), self.host_address)

class Clientkey(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    key = models.CharField(max_length=255)
    expires = models.DateTimeField()
    status = models.CharField(max_length=255, default='') # 'unused', 'connected', 'failed'
    server = models.ForeignKey(Gameserver, default=-1)

    created = models.DateTimeField(auto_now_add=True, default=datetime.datetime.now)
    updated = models.DateTimeField(auto_now=True, default=datetime.datetime.now)

    def __str__(self):
        return "[%s] %s" % (self.user, self.key)

