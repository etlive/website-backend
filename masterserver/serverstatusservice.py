﻿import sys, traceback
import time
import os
import Queue

from datetime import datetime, timedelta
from random import randint

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.internet.task import LoopingCall
from twisted.python import log

import q3util
from models import Gameserver

ETL_MASTER_PORT = int(os.getenv('MASTERSERVER_PORT', 27950))
ET_GETINFO = 'getinfo'
ET_GETSTATUS = 'getinfo' #'getstatus'
TIMEOUT = 10

DEBUG_CHALLENGES = False
VERBOSE = False
DEBUG = False

class ServerinfoReciever(DatagramProtocol):
    resends = {}

    def __init__(self):
        self.resends = {}  # resend last package if no response
        self.packet_prefix = '\xff' * 4
        self.serverinfo_queue = Queue.Queue()
        self.serverdict = {}
        self.numservers = -1
        self.last_index = 0

    def startProtocol(self):
        self.looping_call = LoopingCall(self.get_servers)
        self.looping_call.start(2, now=False)

        self.inforequest_call = LoopingCall(self.send_info_requests)
        self.inforequest_call.start(0.5)

        self.syncdata_call = LoopingCall(self.send_sync_database)
        self.syncdata_call.start(1, now=False)

    def sendMessage(self, message, address, calls=0):
        """
        Sends a message witht the correct prefix
        """
        from twisted.internet import reactor

        # first management of resends of udp-packets
        if address in self.resends:
            task = self.resends[address]
            del self.resends[address]
            try:
                task.cancel()  # send more often than the timeout?
            except:
                pass
        calls += 1
        if calls <= 3:
            self.resends[address] = (reactor.callLater(10, self.sendMessage,
                    message, address, calls=calls))

        # finally send the message
        self.transport.write('%s%s\n' % (self.packet_prefix, message),
                address)

    def send_message(self, host, port, msg): 
        if VERBOSE:
            log.msg('[%s] >> OUT to %s:%s' % (ETL_MASTER_PORT, host, port))
        self.sendMessage(msg, (host, int(port)))
        
    def send_query(self, serveraddr, msg):
        challenge = q3util.build_challenge()
        host, port = serveraddr.split(':')

        # remember the challenge so servers can't be spoofed by malicious servers. Also save the timestamp for diagnostic purposes
        self.serverdict[serveraddr]['challenges'][challenge] = datetime.now()
        
        # send the message
        self.send_message(host, port, msg + " " + challenge)

    def send_infoquery(self, args):
        serveraddr = args['serveraddr']
        self.send_query(serveraddr, ET_GETINFO)

    def send_statusquery(self, serveraddr):
        self.send_query(serveraddr, ET_GETSTATUS)

    def add_querytask(self, addr):
        from twisted.internet import reactor
        # start a new looping call
        new_task = LoopingCall(self.send_statusquery, {'serveraddr': addr})

        self.serverdict[addr]['task'] = new_task
        new_task.start(1, now=False)

    def remove_querytask(self, addr):
        if VERBOSE:
            log.msg('Removing querytask for addr: %s\n' % (addr))
        task = self.serverdict[addr]['task']
        if task is None:
            return

        # stop querying the gameserver
        task.stop()
        del task
        self.serverdict[addr]['task'] = None

    def delete_server(self, addr):
        if VERBOSE:
            log.msg('Deleting server: %s' % addr)
        try:
            servermodel = Gameserver.objects.get(host_address=addr)
            servermodel.delete()
        except Exception as e:
            log.msg('Could not delete server, exception: %s' % e)


    def check_purge(self, addr):
        server_object = self.serverdict[addr]
        if 'purge_count' not in server_object:
            server_object['purge_count'] = 0
        count = server_object['purge_count']

        if count < 10:
            server_object['purge_count'] = count + 1     
        else:  
            if VERBOSE:
                log.msg('Purging server with address: %s'%addr)
            # remove the query task(if any)
            self.remove_querytask(addr)

            # remove the object from the database
            self.delete_server(addr)

            # remove the server_object from the dict
            del self.serverdict[addr]

    # used to check if live servers are really alive, sets status to down otherwise
    def check_timeout(self, addr):
        server_object = self.serverdict[addr]
        if 'reply_timestamp' not in server_object:
            server_object['reply_timestamp'] = datetime.now()

        start = server_object['reply_timestamp']
        elapsed = datetime.now() - start
        
        if elapsed < timedelta(seconds=TIMEOUT):
            return

        try:
            servermodel = Gameserver.objects.get(host_address=addr)
            servermodel.status = 'down' 
            servermodel.save()
        except Exception:
            log.err('Could not update the gameserver model for addr: %s'%addr)
            return

        # finaly remove the query task
        self.remove_querytask(addr)

    
    def get_servers(self):
        # get an entire set of servers(including 'down' servers)
        queryset = Gameserver.objects.all()

        # now filter out the live servers so we can keep track of the number of live servers
        liveservers_count  = queryset.filter(status='up').count()
        if liveservers_count  != self.numservers:
            self.numservers = liveservers_count 
            log.msg('Online servers: %s' % liveservers_count )

        for server in queryset:
            addr = server.host_address
            isdown = (server.status == 'down')
            server_exists = (addr in self.serverdict)
            if not server_exists:
                self.serverdict[addr] = {'task' : None, 'challenges': {}}
            
            if isdown:
                self.check_purge(addr)
            else:
                #if not server_exists:
                #    self.add_querytask(addr)
                #else: 
                self.check_timeout(addr)
                
    def send_info_requests(self):
        max_per_second = 100
        keys = self.serverdict.keys()
        key_count = len(keys)

        if(key_count == 0):
            return

        next_index = min((self.last_index + max_per_second), key_count)
        
        for index in range(self.last_index, next_index):
            self.send_statusquery(keys[index])

        if next_index == self.last_index:
            self.last_index = 0
        else:
            self.last_index = next_index
  
    def stopProtocol(self):
        "Called after all transport is teared down"
        for server_object in self.serverdict.items():
            server_object["task"].stop()
        del self.serverdict

    def parse_packet(self, data):
        cmd, payload = q3util.find_command(data);
        serverdata = {}
        if cmd == 'infoResponse':
            serverdata = q3util.infostring_to_dict(payload)
        elif cmd == 'statusResponse':
            serverdata = q3util.statusresponse_to_dict(payload)
        return serverdata

    def save_serverdata(self, addr, serverdata):
        try:
            servermodel = Gameserver.objects.get(host_address=addr)
        except ObjectDoesNotExist:
            log.err('could not find the gameserver model for addr: %s'%addr)
            return

        servermodel.host_name = serverdata['hostname']
        servermodel.num_players = serverdata['clients']
        servermodel.max_clients = serverdata['sv_maxclients']
        servermodel.gametype = serverdata['gametype']
        servermodel.map_name = serverdata['mapname']
        servermodel.game = serverdata['game']
        servermodel.version = serverdata['version']
        servermodel.needspass = int(serverdata['needpass'])

        try:
            servermodel.save()
        except Exception as e:
            log.err('Could not update server, exception: %s'%e)
    
    def is_valid_challenge(self, addr, challenge):
        serverObject = self.serverdict[addr]
        if challenge in self.serverdict[addr]['challenges']:
            delta = datetime.now() - self.serverdict[addr]['challenges'][challenge]
            if DEBUG_CHALLENGES:
                log.msg('Challenge approved, took: %d ms'%(delta.microseconds / 1000))
            self.serverdict[addr]['challenges'][challenge] = None
            return True
        return False

    def process_packet(self, packet):
        addr = packet['address']
        packetdata = packet['data']
        serverdata = self.parse_packet(packetdata)

        if len(serverdata) == 0:
            log.msg('could not parse packet with data \'%r\''%packetdata)
            return
        
        if not self.is_valid_challenge(addr, serverdata['challenge']):
            log.msg('[%s] server sent an invalid challenge' % addr)
            return

        server_object = self.serverdict[addr]
        server_object['reply_timestamp'] = datetime.now() 
        server_object['data'] = serverdata
           

    def send_sync_database(self):
        for addr in self.serverdict.keys():
            server_object = self.serverdict[addr]
            
            if 'data' not in server_object or len(server_object['data']) == 0:
                continue

            if server_object['reply_timestamp'] < datetime.now() - timedelta(seconds=5):
                continue

            self.save_serverdata(addr, server_object['data'])
        

    def datagramReceived(self, data, (host, port)):
        addr = '%s:%d'%(host, port)
        if VERBOSE:
            log.msg('[%s] << IN from %s' % (ETL_MASTER_PORT, addr))

        if addr not in self.serverdict:
            if VERBOSE:
                log.msg('address "%s" not found in severDict' % addr)
            return
        if data.startswith(self.packet_prefix):
            data = data.lstrip(self.packet_prefix)
            packet = {'data':data, 'address': addr}
            self.process_packet(packet)
        elif VERBOSE: 
            log.msg('packet did not start with prefix, data=%r' % data)
