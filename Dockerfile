FROM python:2
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
ADD ./requirements.txt /code/
WORKDIR /code
RUN pip install --allow-all-external -r requirements.txt
ADD . /code
CMD docker/init_web.sh
