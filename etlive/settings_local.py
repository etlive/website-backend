from settings import *

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'etlive',

        'USER': 'etl_web',
        'PASSWORD': 'etl_web',
        'HOST': 'etlive_db',
        'PORT': '3306'
    }
}
