#!/bin/sh
python manage.py syncdb
python manage.py migrate
echo "from django.contrib.auth import get_user_model; get_user_model().objects.create_superuser('admin', 'admin@example.com', 'pEROmPLusInONS')" | python manage.py shell
python manage.py trunserver 0.0.0.0:8000